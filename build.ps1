New-Item -Type directory -Path binaries | Out-Null
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-Webrequest "https://gitlab.com/kutelev/gitlab-runner/-/jobs/268724494/artifacts/raw/dockerfiles/build/binaries/gitlab-runner-helper.x86_64-windows.exe" -OutFile .\binaries\gitlab-runner-helper.x86_64-windows.exe
docker build -t ${env:CI_REGISTRY}/${env:CI_PROJECT_PATH}:${env:CI_COMMIT_REF_NAME} --build-arg GIT_VERSION=$Env:GIT_VERSION --build-arg GIT_VERSION_BUILD=$Env:GIT_VERSION_BUILD --build-arg GIT_256_CHECKSUM=$Env:GIT_256_CHECKSUM --build-arg GIT_LFS_VERSION=$Env:GIT_LFS_VERSION --build-arg GIT_LFS_256_CHECKSUM=$Env:GIT_LFS_256_CHECKSUM .
docker push ${env:CI_REGISTRY}/${env:CI_PROJECT_PATH}:${env:CI_COMMIT_REF_NAME}
